#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char const *argv[])
{
    int i;    
    if (fork()==0) {
        for(i = 0; i < 100; ++i)
            printf("This is the child process\n");
    }
    else {        
        for(i = 0; i < 100; ++i)
            printf("This is the parent process\n");        
    }
    
    return 0;
}
