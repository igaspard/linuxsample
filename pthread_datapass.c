#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <dirent.h>
#include <string.h>

typedef struct thread_data {    
    char *src;
    char *dst;
    size_t size;
    void *usb_handle;
} thread_data;

void *thread_routine(void *arg) {
    thread_data *data = (thread_data *) arg;
    memcpy(data->dst, data->src, data->size);
    pthread_exit(NULL);
}

int main(int argc, char const *argv[])
{
    pthread_t t;
    void *ret;
    thread_data data;
    
    if (argc < 2) {
        printf("Usage: thread_createfile FILE_PATH\n");
    }

    // List all the files in the current folder
    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    
    if (d) {        
        while ((dir = readdir(d)) != NULL) {            
            if (dir->d_type == DT_REG) {
                printf("File: %s, \n", dir->d_name);
                
                FILE *fp;
                fp = fopen(dir->d_name, "rb");                
                if (fp == NULL) {
                    printf("File %s open failed !!!\n", dir->d_name);
                    exit(1);
                }
                
                if (fseek(fp, 0, SEEK_END)) {
                    printf("Seek the file %s to end failed\n", dir->d_name);
                    exit(1);
                }                
                long file_size = ftell(fp);
                fseek(fp, 0L, SEEK_SET);
                printf("File %s size %ld bytes\n", dir->d_name, file_size);
                
                long fread_size;
                char *src = malloc(sizeof(char)*file_size);
                char *dst = malloc(sizeof(char)*file_size);
                fread_size = fread(src, sizeof(char), file_size, fp);                
                if (fread_size != file_size) {
                    printf("Read file %s failed, fread size return: %ld", dir->d_name, fread_size);
                }

                data.src = src;
                data.dst = dst;
                data.size = sizeof(char) * file_size;
                pthread_create(&t, NULL, thread_routine, (void*) &data);

                pthread_join(t, NULL);                
                if (memcmp(data.src, data.dst, data.size) != 0) {
                    printf("Memory compare failed, thread copy memory didn't work!!!\n");
                }    
                else {
                    printf("Thread Memory Copy PASS !!!\n");
                }
                free(src);
                free(dst);
                fclose(fp);
            }
        }
        closedir(d);
    }

    return 0;
}
