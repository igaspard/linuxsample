# Linux Sample Code
Gaspard Shen

## This git include some test code for linux function.
1. env.c
    * Try the `getenv()` function. 
    * This function can get envrioment variable.
2. fork.c
    * Try the `fork()` function. 
    * This one demo the basic of parent/child process.
3. system.c
    * Try the `system()` function. 
    * This one can call linux command.
4. print-pid.c
    * Try `getpid()`, `getppid()` function. 
    * This one show process ID and parent process ID.
5. fork-exec.c
    * This demostrate the fork with `exec()` family function.
    * exec whith letter **'p'** can search for the program.
    * With letter **'v'** use NULL-terminate array arg.
    * letter 'l' use C language argv.
